import { shallowMount, createLocalVue } from '@vue/test-utils'
import VueRouter from 'vue-router'
import InteractiveMap from '@/components/InteractiveMap'

const localVue = createLocalVue()
localVue.use(VueRouter)

describe('InteractiveMap', () => {
  let wrapper
  let router

  beforeEach(() => {
    router = new VueRouter()

    wrapper = shallowMount(InteractiveMap, {
      router, localVue
    })
  })

  test('should set the correct overlay when item in the menu is clicked', () => {
    wrapper.findAll('.is-item').at(1).trigger('click')

    expect(wrapper.vm.currentOverlay).toBe(wrapper.vm.categories[1].overlay)
  })

  test('should remove the overlay when the item in the menu is clicked again after the overlay has been set', () => {
    wrapper.setData({ currentOverlay: wrapper.vm.categories[1].overlay })

    wrapper.findAll('.is-item').at(1).trigger('click')

    expect(wrapper.vm.currentOverlay).toBeNull()
  })

  test('should not be able to set any overlay when the parent of a sub item in the menu is clicked', () => {
    expect(wrapper.vm.currentOverlay).toBeNull()

    wrapper.findAll('.is-item').at(0).trigger('click')

    expect(wrapper.vm.currentOverlay).toBeNull()
  })

  test('should navigate to organiser when button is clicked', () => {
    const $route = { path: '/organiser' }

    wrapper.find('.goto-organiser').trigger('click')

    expect(wrapper.vm.$route.path).toBe($route.path)
  })
})
