import { shallowMount } from '@vue/test-utils'
import InstructionModal from '@/components/InstructionModal'

describe('InstructionModal', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(InstructionModal)

    wrapper.setProps({ isActive: true })
  })

  test('should emit the closeModal event when button is clicked', () => {
    wrapper.find('button').trigger('click')

    expect(wrapper.emitted().closeModal).toBeTruthy()
  })
})
