import { shallowMount, createLocalVue } from '@vue/test-utils'
import VueRouter from 'vue-router'
import Introduction from '@/components/Introduction'

const localVue = createLocalVue()
localVue.use(VueRouter)

describe('Introduction', () => {
  let wrapper
  let router

  beforeEach(() => {
    router = new VueRouter()

    wrapper = shallowMount(Introduction, {
      router, localVue
    })
  })

  test('should navigate to the interactive map when button is clicked', () => {
    const $route = { path: '/interactive-map' }

    wrapper.find('.goto-interactive-map').trigger('click')

    expect(wrapper.vm.$route.path).toBe($route.path)
  })
})
