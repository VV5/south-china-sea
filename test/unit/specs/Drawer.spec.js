import { shallowMount } from '@vue/test-utils'
import Drawer from '@/components/Drawer'

describe('Drawer', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(Drawer)
  })

  test('should show up arrow when drawer is closed and open drawer when button is clicked', () => {
    wrapper.setData({ showDrawer: false })

    expect(wrapper.vm.type).toBe('up')

    wrapper.find('.is-drawer').trigger('click')

    expect(wrapper.vm.showDrawer).toBe(true)
  })

  test('should show down arrow when drawer is open and close the drawer when button is clicked', () => {
    wrapper.setData({ showDrawer: true })

    expect(wrapper.vm.type).toBe('down')

    wrapper.find('.is-drawer').trigger('click')

    expect(wrapper.vm.showDrawer).toBe(false)
  })
})
