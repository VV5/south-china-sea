import { shallowMount } from '@vue/test-utils'
import Organiser from '@/components/Organiser'
import InstructionModal from '@/components/InstructionModal'

describe('Organiser', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(Organiser)
  })

  test('should close the modal when event is emitted', () => {
    wrapper.setData({ isActive: true })
    expect(wrapper.vm.isActive).toBe(true)

    wrapper.find(InstructionModal).vm.$emit('closeModal')
    wrapper.vm.$emit('closeModal')

    expect(wrapper.emitted().closeModal).toBeTruthy()
    expect(wrapper.vm.isActive).toBe(false)
  })
})
