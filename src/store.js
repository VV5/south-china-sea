import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  state: {
    isLandscape: true
  },
  mutations: {
    setLandscape (state, status) {
      state.isLandscape = status
    }
  }
})
