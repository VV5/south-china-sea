import Vue from 'vue'
import App from '@/App'
import store from '@/store'
import router from '@/router'

require('@mdi/font/scss/materialdesignicons.scss')

Vue.config.productionTip = false

router.beforeEach((to, from, next) => {
  document.title = `${to.name} | South China Sea`

  next()
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  components: { App },
  template: '<App/>'
})
