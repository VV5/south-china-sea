import Vue from 'vue'
import Router from 'vue-router'
import Copyright from '@/components/Copyright'
import Credits from '@/components/Credits'
import Introduction from '@/components/Introduction'
import InteractiveMap from '@/components/InteractiveMap'
import Organiser from '@/components/Organiser'

Vue.use(Router)

export default new Router({
  base: window.location.pathname,
  routes: [
    {
      path: '/',
      name: 'Copyright',
      component: Copyright
    },
    {
      path: '/credits',
      name: 'Credits',
      component: Credits
    },
    {
      path: '/introduction',
      name: 'Introduction',
      component: Introduction
    },
    {
      path: '/interactive-map',
      name: 'InteractiveMap',
      component: InteractiveMap
    },
    {
      path: '/organiser',
      name: 'Organiser',
      component: Organiser
    }
  ]
})
